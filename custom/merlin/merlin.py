# -*- coding: utf-8 -*-
"""
Merlin
version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import os
import shutil
import subprocess
import sys
import time


def __main__():

    ex_path = 'merlin'

    # load arguments
    print 'Parsing Merlin input options...'
    parser = optparse.OptionParser()
    
    parser.add_option('--data_files', action='append', dest='data_files', help='')
    parser.add_option('--pedigree_files', action='append', dest='pedigree_files', help='')
    parser.add_option('--map_file', dest='map_file', help='')
    parser.add_option('--allele_frequency', dest='allele_frequency', help='')
    parser.add_option('--missing_value_code', dest='missing_value_code', help='')
    parser.add_option('--seed', dest='seed', help='')
    
    parser.add_option('--error', action="store_true", dest='error', help='')
    parser.add_option('--information', action="store_true", dest='information', help='')
    parser.add_option('--likelihood', action="store_true", dest='likelihood', help='')
    parser.add_option('--model_file', dest='model_file', help='')
    
    parser.add_option('--ibd', action="store_true", dest='ibd', help='')
    parser.add_option('--kinship', action="store_true", dest='kinship', help='')
    parser.add_option('--matrices', action="store_true", dest='matrices', help='')
    parser.add_option('--extended', action="store_true", dest='extended', help='')
    parser.add_option('--select', action="store_true", dest='select', help='')
    
    parser.add_option('--npl', action="store_true", dest='npl', help='')
    parser.add_option('--pairs', action="store_true", dest='pairs', help='')
    parser.add_option('--qtl', action="store_true", dest='qtl', help='')
    parser.add_option('--deviates', action="store_true", dest='deviates', help='')
    parser.add_option('--exp', action="store_true", dest='exp', help='')
    parser.add_option('--zscores', action="store_true", dest='zscores', help='')
    
    parser.add_option('--vc', action="store_true", dest='vc', help='')
    parser.add_option('--use_covariates', action="store_true", dest='use_covariates', help='')
    parser.add_option('--ascertainment', action="store_true", dest='ascertainment', help='')
    parser.add_option('--unlinked', dest='unlinked', help='')
    
    parser.add_option('--infer', action="store_true", dest='infer', help='')
    parser.add_option('--assoc', action="store_true", dest='assoc', help='')
    parser.add_option('--fast_assoc', action="store_true", dest='fast_assoc', help='')
    parser.add_option('--filter', dest='filter', help='')
    parser.add_option('--custom_file', dest='custom_file', help='')
    
    parser.add_option('--steps', dest='steps', help='')
    parser.add_option('--min_step', dest='min_step', help='')
    parser.add_option('--max_step', dest='max_step', help='')
    parser.add_option('--grid', dest='grid', help='')
    parser.add_option('--start', dest='start', help='')
    parser.add_option('--stop', dest='stop', help='')
    parser.add_option('--positions', dest='positions', help='')
    
    parser.add_option('--best', action="store_true", dest='best', help='')
    parser.add_option('--sample', action="store_true", dest='sample', help='')
    parser.add_option('--all', action="store_true", dest='all', help='')
    parser.add_option('--sample_n', dest='sample_n', help='')
    parser.add_option('--founders', action="store_true", dest='founders', help='')
    parser.add_option('--horizontal', action="store_true", dest='horizontal', help='')
    
    parser.add_option('--recombinations', dest='recombinations', help='')
    parser.add_option('--singlepoint', action="store_true", dest='singlepoint', help='')
    
    parser.add_option('--cluster_file', dest='cluster_file', help='')
    parser.add_option('--distance', dest='distance', help='')
    parser.add_option('--rsq', dest='rsq', help='')
    parser.add_option('--cfreq', action="store_true", dest='cfreq', help='')
    
    parser.add_option('--bits', dest='bits', help='')
    parser.add_option('--megabytes', dest='megabytes', help='')
    parser.add_option('--minutes', dest='minutes', help='')
    
    parser.add_option('--trim', action="store_true", dest='trim', help='')
    parser.add_option('--no_couple_bits', action="store_true", dest='no_couple_bits', help='')
    parser.add_option('--swap', action="store_true", dest='swap', help='')
    parser.add_option('--small_swap', action="store_true", dest='small_swap', help='')
    
    parser.add_option('--quiet', action="store_true", dest='quiet', help='')
    parser.add_option('--marker_names', action="store_true", dest='marker_names', help='')
    parser.add_option('--frequencies', action="store_true", dest='frequencies', help='')
    parser.add_option('--per_family', action="store_true", dest='per_family', help='')
    parser.add_option('--pdf', action="store_true", dest='pdf', help='')
    parser.add_option('--tabulate', action="store_true", dest='tabulate', help='')
    
    parser.add_option('--simulate', action="store_true", dest='simulate', help='')
    parser.add_option('--reruns', dest='reruns', help='')
    parser.add_option('--trait', dest='trait', help='')
    parser.add_option('--save', action="store_true", dest='save', help='')
    parser.add_option('--inverse_normal', action="store_true", dest='inverse_normal')
    
    parser.add_option('--logfile', dest='logfile', help='')
    parser.add_option('--output_id', dest='output_id')
    parser.add_option('--new_file_path', dest='new_file_path')

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Merlin command to be executed
    # data file(s)

    data_files = '-d '
    for item in options.data_files:
        data_files += '%s,' % (item)
    data_files = data_files[:-1]
    
    pedigree_files = '-p '
    for item in options.pedigree_files:
        pedigree_files += '%s,' % (item)
    pedigree_files = pedigree_files[:-1]

    map_file = '-m %s' % (options.map_file)
    allele_frequency = '-f %s' % (options.allele_frequency) if options.allele_frequency is not None else ''
    missing_value_code = '-x %s' % (options.missing_value_code) if options.missing_value_code is not None else ''
    seed = '-r %s' % (options.seed) if options.seed is not None else ''

    error = '--error' if options.error is not None else ''
    information = '--information' if options.information is not None else ''
    likelihood = '--likelihood' if options.likelihood is not None else ''
    model_file = '--model %s' % (options.model_file) if options.model_file is not None else ''

    ibd = '--ibd' if options.ibd is not None else ''
    kinship = '--kinship' if options.kinship is not None else ''
    matrices = '--matrices' if options.matrices is not None else ''
    extended = '--extended' if options.extended is not None else ''
    select = '--select' if options.select is not None else ''

    npl = '--npl' if options.npl is not None else ''
    pairs = '--pairs' if options.pairs is not None else ''
    qtl = '--qtl' if options.qtl is not None else ''
    deviates = '--deviates' if options.deviates is not None else ''
    exp = '--exp' if options.exp is not None else ''
    zscores = '--zscores' if options.zscores is not None else ''

    vc = '--vc' if options.vc is not None else ''
    use_covariates = '--useCovariates' if options.use_covariates is not None else ''
    ascertainment = '--ascertainment' if options.ascertainment is not None else ''
    unlinked = '--unlinked %s' % (options.unlinked) if options.unlinked is not None else ''

     # parameter
#     = '-- %s' % (options.) if options. is not None else ''
     # boolean
#     = '--' if options. is not None else ''

    infer = '--infer' if options.infer is not None else ''
    assoc = '--assoc' if options.assoc is not None else ''
    fast_assoc = '--fastAssoc' if options.fast_assoc is not None else ''
    filter_p = '--filter %s' % (options.filter) if options.filter is not None else ''
    custom_file = '--custom %s' % (options.custom_file) if options.custom_file is not None else ''

    steps = '--steps %s' % (options.steps) if options.steps is not None else ''
    min_step = '--minStep %s' % (options.min_step) if options.min_step is not None else ''
    max_step = '--maxStep %s' % (options.max_step) if options.max_step is not None else ''
    grid = '--grid %s' % (options.grid) if options.grid is not None else ''
    start = '--start %s' % (options.start) if options.start is not None else ''
    stop = '--stop %s' % (options.stop) if options.stop is not None else ''
    positions = '--positions %s' % (options.positions) if options.positions is not None else ''

    best = '--best' if options.best is not None else ''
    sample = '--sample' if options.sample is not None else ''
    all_p = '--all' if options.all is not None else ''
    sample_n = '--sample %s' % (options.sample_n) if options.sample_n is not None else ''
    founders = '--founders' if options.founders is not None else ''
    horizontal = '--horizontal' if options.horizontal is not None else ''

    recombinations = '--%s' % (options.recombinations) if options.recombinations is not None else ''
    singlepoint = '--singlepoint' if options.singlepoint is not None else ''

    cluster_file = '--cluster %s' % (options.cluster_file) if options.cluster_file is not None else ''
    distance = '--distance %s' % (options.distance) if options.distance is not None else ''
    rsq = '--rsq %s' % (options.rsq) if options.rsq is not None else ''
    cfreq = '--cfreq' if options.cfreq is not None else ''

    bits = '--bits %s' % (options.bits) if options.bits is not None else ''
    megabytes = '--megabytes %s' % (options.megabytes) if options.megabytes is not None else ''
    minutes = '--minutes %s' % (options.minutes) if options.minutes is not None else ''

    trim = '--trim' if options.trim is not None else ''
    no_couple_bits = '--noCoupleBits' if options.no_couple_bits is not None else ''
    swap = '--swap' if options.swap is not None else ''
    small_swap = '--smallSwap' if options.small_swap is not None else ''

    quiet = '--quiet' if options.quiet is not None else ''
    marker_names = '--markerNames' if options.marker_names is not None else ''
    frequencies = '--frequencies' if options.frequencies is not None else ''
    per_family = '--perFamily' if options.per_family is not None else ''
    pdf = '--pdf' if options.pdf is not None else ''
    tabulate = '--tabulate' if options.tabulate is not None else ''

    simulate = '--simulate' if options.simulate is not None else ''
    reruns = '--reruns %s' % (options.reruns) if options.reruns is not None else ''
    trait = '--trait %s' % (options.trait) if options.trait is not None else ''
    save = '--save' if options.save is not None else ''
    inverse_normal = '--inverseNormal' if options.inverse_normal is not None else ''

    # output file(s)
    logfile = options.logfile
    logfile2 = 'tmp_log_file.log'
    output_id = options.output_id
    new_file_path = options.new_file_path    




    # Build Merlin command
    cmd = '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (ex_path, data_files, pedigree_files, map_file, allele_frequency, missing_value_code, seed, error, information, likelihood, model_file, ibd, kinship, matrices, extended, select, npl, pairs, qtl, deviates, exp, zscores, vc, use_covariates, ascertainment, unlinked, infer, assoc, fast_assoc, filter_p, custom_file, steps, min_step, max_step, grid, start, stop, positions, best, sample, all_p, sample_n, founders, horizontal, recombinations, singlepoint, cluster_file, distance, rsq, cfreq, bits, megabytes, minutes, trim, no_couple_bits, swap, small_swap, quiet, marker_names, frequencies, per_family, pdf, tabulate, simulate, reruns, trait, save, inverse_normal)
    print '\nMerlin command to be executed:\n %s' % (cmd)

    # Execution of Merlin
    print 'Executing Merlin...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because Merlin writes some logging info there

        cwd = os.getcwd()
        files = sorted([ f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith('merlin')) ])
        it = 0
        for f in files:
            file_extension = f.split('.')[-1]
            if file_extension == 'pdf':
                file_type = 'pdf'
            else:
                file_type = 'txt'
            new_file = "%s/%s_%s_%s_%s_%s" % ( new_file_path, 'primary', output_id, f, 'visible', file_type )
            s = subprocess.check_call(['mv', f, new_file])
            print "Successfully moved %s to %s: return %s" % (f, new_file, s)
            it = it + 1
        print "Successfully moved %d of %d files!" % (it, len(files))
        
    finally:

        if log != sys.stdout:
            log.close()
    print 'Merlin executed!'
    

if __name__ == "__main__":
    __main__()
