library(ExomeDepth)
#data(exons.hg19)
#data(exons.hg19.X)
## Import parameters from xml script(args_file)
args<-commandArgs(trailingOnly=TRUE)
param<-read.table(args[1],sep="=", as.is=TRUE)                             ##read args_file
## Create symbolic links for bam and bai Test files
file.symlink(param[match("test",param[,1]),2], "test.bam")
file.symlink(param[match("test_bai",param[,1]),2], "test.bam.bai")
test.bam<-"test.bam"
target<-param[match("target",param[,1]),2]
trans.prob<-as.numeric(param[match("trans",param[,1]),2])
#over.Conrad<-as.numeric(param[match("ov_con",param[,1]),2])
#over.Exons<-as.numeric(param[match("ov_hg",param[,1]),2])
output.file<-param[match("out",param[,1]),2]
ref_id<-which(param[,1]=="ref")
ref.bam<-paste("ref",1:length(ref_id),".bam",sep="")
ref_bai<-paste("ref",1:length(ref_id),".bam.bai",sep="")
## Create symbolic links for multiple bam and bai Reference files 
for(i in 1:length(ref_id)){
  file.symlink(param[ref_id[i],2], ref.bam[i])
  file.symlink(param[ref_id[i]+1,2], ref_bai[i])
}
## Generate read count data for the autosomes and the chromosome X
#exons.hg19.full<-rbind(exons.hg19,exons.hg19.X)
set.counts<-getBamCounts(bed.file=target, 
                         bam.files=c(test.bam,ref.bam))
ExomeCount.dafr<-as(set.counts[, colnames(set.counts)], 'data.frame')        ##set.counts forced in a data frame
ExomeCount.dafr$chromosome<-gsub(as.character(ExomeCount.dafr$space),
                                 pattern='chr',
                                 replacement='')

### Prepare the main matrix of read count data
set.matrix <- as.matrix( ExomeCount.dafr[, grep(names(ExomeCount.dafr), pattern='.bam')])
if(is.matrix(set.matrix[,-1])){                                             ##if the reference set is a matrix (made from multiple files)
 ref.counts <- set.matrix[,-1]
}else{                                                                      ##else force a vector into a matrix
  ref.counts<-as.matrix(set.matrix[,-1])
  dimnames(ref.counts) <- list(NULL, colnames(set.matrix)[-1])
}
#### Create the aggregate reference set for this sample
choice <- select.reference.set (test.counts = set.matrix[,1],  
                                reference.counts =ref.counts,  
                                bin.length = (ExomeCount.dafr$end - ExomeCount.dafr$start)/1000,
                                n.bins.reduced = 10000)

reference.selected <- apply(X = set.matrix[,choice$reference.choice, drop=FALSE],
                               MAR=1,
                               FUN=sum)
########## Now creating the ExomeDepth object for the CNVs call
all.exons<-new('ExomeDepth',
               test=set.matrix[,1],
               reference=reference.selected,
               formula= 'cbind(test,reference)~1')


################ Now call the CNVs
all.exons<-CallCNVs(x=all.exons,
                    transition.probability=trans.prob,
                    chromosome= ExomeCount.dafr$space,
                    start= ExomeCount.dafr$start,
                    end=ExomeCount.dafr$end,
                    name=ExomeCount.dafr$names)

write.table(sep='\t',quote=FALSE, file = output.file, x = all.exons@CNV.calls[,c(7,5,6,3,9,10,11,12)], row.names = FALSE, dec=",")
message('Output file created')
########################### Now annotate the ExomeDepth object
# data(Conrad.hg19)
# if(nrow(all.exons@CNV.calls)>0){                                          ##if the object is not empty
#   all.exons@CNV.calls$chromosome<-gsub(as.character(all.exons@CNV.calls$chromosome),
#                                        pattern = 'chr',
#                                        replacement = '')
# ## Get annotation dataset
#   exons.hg19.GRanges <- GRanges(seqnames = exons.hg19.full$chromosome,
#                                   IRanges(start=exons.hg19.full$start,end=exons.hg19.full$end),
#                                   names = exons.hg19.full$name)
# ## Annotation for common CNVs by Conrad et al, Nature 2010
# 
#   all.exons<-AnnotateExtra(x=all.exons,
#                            reference.annotation= Conrad.hg19.common.CNVs,
#                            min.overlap=over.Conrad,
#                            column.name='Conrad.hg19')
# ## Annotation from exons.hg19
#   all.exons <- AnnotateExtra(x = all.exons,
#                              reference.annotation = exons.hg19.GRanges,
#                              min.overlap = over.Exons,
#                              column.name = 'exons.hg19.full')

 
#}else{message('CNV not found')}
