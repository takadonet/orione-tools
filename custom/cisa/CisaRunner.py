import os
import string
import sys
import re
import argparse



class FileIntegrator:
    def __init__(self,fs):
        self.master=open(fs,'w')
    def append(self,fi):
        con=[]
        f=open(fi)
        con=f.readlines()
        self.master.writelines(con)
        f.close()
    def close(self):
        self.master.close()
        
def N50(data_path,f_n50):
    f=open(data_path+'.p.fa')
    d=dict()
    genome=0
    while True:
        h=f.readline()
        if not h:break
        seq=f.readline().replace('\n','')
        d[h]=int(h.split(':')[1])
    f.close()
    d_s=sorted(d.values(),key=lambda x:x,reverse=True)
    for value in d_s:
        genome+=value
    print 'whole:%d'%genome
    f_n50.write('Whole Genome : '+str(genome)+'\n')
    n5=genome/2
    genome=0
    for row in d_s:
        genome+=row
        if genome > n5:
            print 'N50: %d' % row
            f_n50.write('N50 : '+str(row)+'\n')
            break
            
def convert_title(data_path,title,min_len,f_n50):
    f=open(data_path+'.One_l')
    fw=open(data_path+'.p.fa','w')
    min_len=int(min_len)
    max_len=0
    count=1
    while True:
        h=f.readline().replace('\n','')
        if not h:break
        s=f.readline().replace('\n','').upper()
        if len(s)<min_len:continue
        fw.write('>'+title+'_'+str(count)+'_len:'+str(len(s))+'\n')
        fw.write(s+'\n')
        if len(s)>max_len:max_len=len(s)
        count+=1
    print 'Number of contigs: %d'%(count-1)
    print 'Length of the longest contig: %d'%max_len
    f_n50.write('Number of contigs: '+str(count-1)+'\n')
    f_n50.write('Length of the longest contig: '+str(max_len)+'\n')
    f.close();fw.close()
    
def multi2one(data_path):
    f=open(data_path)
    fw=open(data_path+'.One_l','w')
    temp=''
    for s in f:
        s=s.replace('\n','')
        if '>' in s:
            if temp:fw.write(temp+'\n');temp=''
            header=s
            fw.write(header+'\n')
        else:
            temp=temp+s
    fw.write(temp+'\n')
    f.close();fw.close()
    
def Process_Gap(data_path,title,gaps,min_len,f_n50):
    f=open(data_path+'.One_l')
    fw=open(data_path+'.p.fa','w')
    min_len=int(min_len)
    max_len=0
    count=1
    while True:
        h=f.readline().replace('\n','')
        if not h:break
        s=f.readline().replace('\n','').upper()
        if 'N' in s:
            #t_s=re.sub('N+','N',s).split('N')
            t_s=re.sub('N{'+gaps+',}','=CUT=',s).split('=CUT=') # Up 10 continue N to be cut
            for data in t_s:
                if len(data)<min_len:continue
                fw.write('>'+title+'_'+str(count)+'_len:'+str(len(data))+'\n')
                fw.write(data+'\n')
                if len(data)>max_len:max_len=len(data)
                count+=1
        else:
            if len(s)<min_len:continue
            fw.write('>'+title+'_'+str(count)+'_len:'+str(len(s))+'\n')
            fw.write(s+'\n')
            if len(s)>max_len:max_len=len(s)
            count+=1
    print 'Number of contigs: %d'%(count-1)
    print 'Length of the longest contig: %d'%max_len
    f_n50.write('Number of contigs: '+str(count-1)+'\n')
    f_n50.write('Length of the longest contig: '+str(max_len)+'\n')
    fw.close();f.close()



class CisaRunner:
	
	def __init__(self, minlength, gaps, outfile, genomesize, contigsfiles):
		self.min_len = minlength
		self.gaps = gaps
		self.master_file = outfile+ ".merged"
		self.outfile = outfile
		self.genomesize = genomesize
		self.contigsfiles = contigsfiles
		self.count = len(contigsfiles)
		#
		self.nucmer = 'nucmer'
		self.CISA = os.path.dirname(os.path.realpath(__file__))
		self.CISA_S = self.CISA+'/src'
		self.makeblastdb = 'makeblastdb'
		self.blastn = 'blastn'
		return
	
	def run(self):
		""" """
		self.runMerge()
		self.runCisa()
		os.remove(self.master_file)
		return
	
	
	def runMerge(self):
		""" """
		mf = FileIntegrator(self.master_file)
		f_n50 = open('Merge_info','w') # spostare in un file di log
		
		for contigsfilename in self.contigsfiles: # oppure lasciare self.data
			data_path = contigsfilename.strip() #  i.split(',')[0].split('=')[1]
			title =  contigsfilename.rsplit(".", 1)[0].strip() #  i.split(',')[1].split('=')[1]
			multi2one(data_path)
			print data_path+'.p.fa'
			f_n50.write(data_path+'.p.fa'+'\n')
			if self.gaps=='0':
				convert_title(data_path, title, self.min_len, f_n50)
			else:
				Process_Gap(data_path, title, self.gaps, self.min_len, f_n50)
			mf.append(data_path+'.p.fa')
			N50(data_path, f_n50)
			os.remove(data_path+'.One_l');#os.remove(data_path+'.p.fa')
		mf.close()
		f_n50.close()
		return
	
	def runCisa(self):
		""" """
		r2_gap = '0.95'   # passare da input
		outfile = self.outfile  # passare da input
		infile = self.master_file
			
		# round 1		
		current_p = os.getcwd()
		sys.path.append(self.CISA_S+'/CISA1')
		from Controllor_R1 import R1
		if not os.path.exists('CISA1'):os.makedirs('CISA1')
		my_work = R1()
		my_work.Start(self.genomesize, infile, self.nucmer, self.CISA_S, current_p)
		
		# round 2
		sys.path.append(self.CISA_S+'/CISA2')
		if not os.path.exists('CISA2'):os.makedirs('CISA2')
		from Controllor_R2 import R2
		my_work=R2()
		my_work.Start(self.nucmer, r2_gap, self.CISA_S, current_p)
		
		# round 3
		sys.path.append(self.CISA_S+'/CISA3')
		if not os.path.exists('CISA3'):os.makedirs('CISA3')
		from Controllor_R3 import R3
		my_work=R3()
		my_work.Start(self.CISA_S, current_p, self.makeblastdb, self.blastn)
		
		# log ?
		f = open('info1')
		for data in f:
			if 'Round3_result=' in data:R4_start=data.split('=')[1].replace('\n','')
			if 'Repeat_region=' in data:thr=data.split('=')[1].replace('\n','')
		f.close()
		
		# 
		final = R4_start
		if int(thr)>0:
			sys.path.append(self.CISA_S+'/CISA4')
			if not os.path.exists('CISA4'):os.makedirs('CISA4')
			from Controllor_R4 import R4
			my_work = R4()
			my_work.Start(thr, R4_start, self.CISA_S, current_p, self.makeblastdb, self.blastn)
			f = open('info2')
			for data in f:
				if 'Round4_result=' in data:R4_end=data.split('=')[1].replace('\n','')
			f.close()
			if R4_end:final=R4_end

		os.rename('Contigs_'+final+'.fa',outfile)
		final=int(final)
		for i in range(1,final):
			if os.path.exists('Contigs_'+str(i)+'.fa'):os.remove('Contigs_'+str(i)+'.fa')
		return
	




# END OF CLASS
	
if __name__ == "__main__":
	minlength = sys.argv[1] # def 100
	gaps = sys.argv[2]        # def 11
	outfile = sys.argv[3] # da non confondere con il merged
	genomesize = sys.argv[4]
	contigsfiles = sys.argv[5:]
	CR = CisaRunner(minlength, gaps, outfile, genomesize,contigsfiles)
	CR.run()
