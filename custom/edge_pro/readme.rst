EDGE-pro wrapper
================

Dependencies of EDGE-pro which need to be installed separetely
--------------------------------------------------------------

- Switch_ Perl module.

.. _Switch: http://search.cpan.org/~rgarcia/Switch/Switch.pm

Configuration
-------------

edge_pro tool may be configured to use more than one CPU core by selecting an appropriate destination for this tool in Galaxy job_conf.xml file (see https://wiki.galaxyproject.org/Admin/Config/Jobs and https://wiki.galaxyproject.org/Admin/Config/Performance/Cluster ).

If you are using Galaxy release_2013.11.04 or later, this tool will automatically use the number of CPU cores allocated by the job runner according to the configuration of the destination selected for this tool.

If instead you are using an older Galaxy release, you should also add a line

  GALAXY_SLOTS=N; export GALAXY_SLOTS

(where N is the number of CPU cores allocated by the job runner for this tool) to the file

  <tool_dependencies_dir>/edge-pro/1.3.1/crs4/edge_pro/<hash_string>/env.sh

Version history
---------------

- Unreleased: Update Orione citation.
- Release 2: Directly call edge.pl, remove edge_pro.py . Move minimun and maximum insert size params to paired library section. Update Orione citation.
- Release 1: Use $GALAXY_SLOTS instead of $EDGE_PRO_SITE_OPTIONS. Add dependency on bowtie2. Add readme.rst .
- Release 0: Initial release in the Tool Shed.

Development
-----------

Development is hosted at https://bitbucket.org/crs4/orione-tools . Contributions and bug reports are very welcome!
