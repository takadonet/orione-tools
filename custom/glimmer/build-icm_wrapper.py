# -*- coding: utf-8 -*-
"""
Glimmer --> build-icm
version 0.2 (andrea.pinna@crs4.it)
"""

import optparse
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing Build-ICM input options...'
    parser = optparse.OptionParser()
    parser.add_option('--biSequence', dest='sequence', help='')
    parser.add_option('--biDepth', dest='depth', type='int', help='')
    parser.add_option('--biNoStops', action='store_true', dest='no_stops', help='')
    parser.add_option('--biPeriod', dest='period', type='int', help='')
    parser.add_option('--biReverse', action='store_true', dest='reverse', help='')
    parser.add_option('--biWidth', dest='width', type='int', help='')
    parser.add_option('--biTransTable', dest='trans_table', type='int', help='')
    parser.add_option('--biStopCodons', dest='stop_codons', help='')
    parser.add_option('--biIcm', dest='output', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Build-ICM command to be executed
    sequence = options.sequence
    depth = '--depth %d' % (options.depth) if options.depth is not None else ''
    no_stops = '--no_stops' if options.no_stops else ''
    period = '--period %d' % (options.period) if options.period is not None else ''
    reverse = '--reverse' if options.reverse else ''
    width = '--width %d' % (options.width) if options.width is not None else ''
    trans_table = '--trans_table %d' % (options.trans_table) if options.trans_table is not None else ''
    stop_codons = '--stop_codons %s' % (options.stop_codons) if options.stop_codons else ''
    output = options.output
    logfile = options.logfile

    # Build Build-ICM command
    cmd = 'build-icm %s %s %s %s %s %s %s %s < %s ' % (depth, no_stops, period, reverse, width, trans_table, stop_codons, output, sequence)
    print '\nBuild-ICM command to be executed:\n %s' % (cmd)

    print 'Executing Build-ICM...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'Build-ICM executed!'


if __name__ == "__main__":
    __main__()
