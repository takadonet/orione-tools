# -*- coding: utf-8 -*-
"""
Mach
version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import os
import shutil
import subprocess
import sys


def __main__():

    ex_path = 'mach1'

    # load arguments
    print 'Parsing Mach input options...'
    parser = optparse.OptionParser()
    
    parser.add_option('--data_files', action='append', dest='data_files')
    parser.add_option('--pedigree_files', action='append', dest='pedigree_files')
    parser.add_option('--mask', dest='mask')
    parser.add_option('--crossover_map_file', dest='crossover_map_file')
    parser.add_option('--error_map_file', dest='error_map_file')
    parser.add_option('--physical_map_file', dest='physical_map_file')
    parser.add_option('--haps_file', dest='haps_file')
    parser.add_option('--snps_file', dest='snps_file')
    
    parser.add_option('--hapmap_format', action="store_true", dest='hapmap_format')
    parser.add_option('--vcf_reference', action="store_true", dest='vcf_reference')
    parser.add_option('--autoflip', action="store_true", dest='autoflip')
    parser.add_option('--greedy', action="store_true", dest='greedy')
    
    parser.add_option('--start_position', dest='start_position')
    parser.add_option('--end_position', dest='end_position')
    parser.add_option('--seed', dest='seed')
    parser.add_option('--burnin', dest='burnin')
    parser.add_option('--rounds', dest='rounds')
    
    parser.add_option('--npl', action="store_true", dest='npl')
    parser.add_option('--association', action="store_true", dest='association')
    
    parser.add_option('--states', dest='states')
    parser.add_option('--error_rate', dest='error_rate')
    
    parser.add_option('--weighted', action="store_true", dest='weighted')
    parser.add_option('--compact', action="store_true", dest='compact')
    parser.add_option('--force_imputation', action="store_true", dest='force_imputation')
    parser.add_option('--geno', action="store_true", dest='geno')
    parser.add_option('--quality', action="store_true", dest='quality')
    parser.add_option('--dosage', action="store_true", dest='dosage')
    parser.add_option('--probs', action="store_true", dest='probs')
    parser.add_option('--mle', action="store_true", dest='mle')
    parser.add_option('--phase', action="store_true", dest='phase')
    parser.add_option('--uncompressed', action="store_true", dest='uncompressed')
    parser.add_option('--mldetails', action="store_true", dest='mldetails')
    
    parser.add_option('--sample_interval', dest='sample_interval')
    parser.add_option('--interim_interval', dest='interim_interval')
    
    parser.add_option('--logfile', dest='logfile')
    parser.add_option('--output_id', dest='output_id')
    parser.add_option('--new_file_path', dest='new_file_path')
    

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Mach command to be executed
    # data file(s)

    data_files = '-d '
    for item in options.data_files:
        data_files += '%s,' % (item)
    data_files = data_files[:-1] # remove last comma
    
    pedigree_files = '-p '
    for item in options.pedigree_files:
        pedigree_files += '%s,' % (item)
    pedigree_files = pedigree_files[:-1] # remove last comma
    
    mask = '--mask %s' % (options.mask) if options.mask is not None else '' 
    crossover_map_file = '--crossoverMap %s' % (options.crossover_map_file) if options.crossover_map_file is not None else ''
    error_map_file = '--errorMap %s' % (options.error_map_file) if options.error_map_file is not None else ''
    physical_map_file = '--physicalMap %s' % (options.physical_map_file) if options.physical_map_file is not None else ''
    haps_file = '-h %s' % (options.haps_file) if options.haps_file is not None else ''
    snps_file = '-s %s' % (options.snps_file) if options.snps_file is not None else ''
     
    hapmap_format = '--hapmapFormat' if options.hapmap_format is not None else ''
    vcf_reference = '--vcfReference' if options.vcf_reference is not None else ''
    autoflip = '--autoFlip' if options.autoflip is not None else ''
    greedy = '--greedy' if options.greedy is not None else ''
     
    start_position = '--startposition %s' % (options.start_position) if options.start_position is not None else ''
    end_position = '--endposition %s' % (options.end_position) if options.end_position is not None else ''
    seed = '--seed %s' % (options.seed) if options.seed is not None else ''
    burnin = '--burnin %s' % (options.burnin) if options.burnin is not None else ''
    rounds = '--rounds %s' % (options.rounds) if options.rounds is not None else ''
     
    npl = '--npl' if options.npl is not None else ''
    association = '--association' if options.association is not None else ''
     
    states = '--states %s' % (options.states) if options.states is not None else ''
    error_rate = '--errorRate %s' % (options.error_rate) if options.error_rate is not None else ''
     
    weighted = '--weighted' if options.weighted is not None else ''
    compact = '--compact' if options.compact is not None else ''
    force_imputation = '--forceImputation' if options.force_imputation is not None else ''
    geno = '--geno' if options.geno is not None else ''
    quality = '--quality' if options.quality is not None else ''
    dosage = '--dosage' if options.dosage is not None else ''
    probs = '--probs' if options.probs is not None else ''
    mle = '--mle' if options.mle is not None else ''
    phase = '--phase' if options.phase is not None else ''
    uncompressed = '--uncompressed' if options.uncompressed is not None else ''
    mldetails = '--mldetails' if options.mldetails is not None else ''

    sample_interval = '--sampleInterval %s' % (options.sample_interval) if options.sample_interval is not None else ''
    interim_interval = '--interimInterval %s' % (options.interim_interval) if options.interim_interval is not None else ''
    # output file(s)
    logfile = options.logfile
    output_id = options.output_id
    new_file_path = options.new_file_path
    
    
    # Build Mach command
    cmd = '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (ex_path, data_files, pedigree_files, mask, crossover_map_file, error_map_file, physical_map_file, haps_file, snps_file, hapmap_format, vcf_reference, autoflip, greedy, start_position, end_position, seed, burnin, rounds, npl, association, states, error_rate, weighted, compact, force_imputation, geno, quality, dosage, probs, mle, phase, uncompressed, mldetails, sample_interval, interim_interval)
    print '\nMach command to be executed:\n %s' % (cmd)

    # Execution of Mach
    print 'Executing Mach...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because Mach writes some logging info there

        # Get current working directory
        cwd = os.getcwd()
        # Obtain compressed files
        gz_files = sorted([ f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith('mach1') and f.endswith('.gz')) ])
        # Uncompress compressed files
        for fz in gz_files:
            gz_cmd = 'gunzip %s' % fz
            s = subprocess.check_call(gz_cmd, shell=True)
            print "Successfully uncompressed %s to %s: return %s" % (fz, fz[:-3], s)
        # Obtain all files produced by MACH
        files = sorted([ f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith('mach1')) ])
        # Rename files as requested to appear as output datasets
        it = 0
        for f in files:
            file_extension = f.split('.')[-1]
            if file_extension == 'pdf':
                file_type = 'pdf'
            else:
                file_type = 'txt'
            new_file = "%s/%s_%s_%s_%s_%s" % ( new_file_path, 'primary', output_id, f, 'visible', file_type )
            s = subprocess.check_call(['mv', f, new_file])
            print "Successfully moved %s to %s: return %s" % (f, new_file, s)
            it = it + 1
        print "Successfully moved %d of %d files!" % (it, len(files))

    finally:
        if log != sys.stdout:
            log.close()
    print 'Mach executed!'
    

if __name__ == "__main__":
    __main__()
