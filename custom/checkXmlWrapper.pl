#!/usr/bin/perl -w
# Copyright © 2013-2014 CRS4 Srl. http://www.crs4.it/
# Created by:
# Paolo Uva <paolo.uva@crs4.it>
# Nicola Soranzo <nicola.soranzo@crs4.it>
#
# Released under the MIT license http://opensource.org/licenses/MIT .

use strict;

use XML::Simple;
use Data::Dumper;
use Scalar::Util qw(looks_like_number);

binmode(STDOUT, ':utf8');

die 'Missing argument' if (! @ARGV);
if (-d $ARGV[0]) {
    recurse($ARGV[0]);
} elsif (-f $ARGV[0]) {
    checkXML($ARGV[0]);
} else {
    die 'File or directory not found'
}

sub checkSelectParam {
    my $select_name = $_[0];
    my $select_hash = $_[1];

    if (exists($select_hash->{'multiple'}) and lc($select_hash->{'multiple'}) eq 'false') {
        print ', REMOVE REDUNDANT multiple="false"';
    }
    if ((! exists($select_hash->{'multiple'}) or lc($select_hash->{'multiple'}) eq 'false') and exists($select_hash->{'optional'})) {
        print ', REMOVE REDUNDANT optional';
    }
}

sub checkParamContainer {
    my $param_container = $_[0];

    if (exists($param_container->{'param'})) {
        # $param_container->{'param'} is a hash reference
        while (my ($param_name, $param_hash) = each(%{ $param_container->{'param'} })) {
            print '- ', $param_name, ': ';
            if (! exists($param_hash->{'type'})) {
                print 'MISSING type';
            } else {
                print 'type "', $param_hash->{'type'}, '"';
                if ($param_hash->{'type'} eq 'data') {
                    print ', ';
                    if (exists($param_hash->{'format'})) {
                        print $param_hash->{'format'} eq '' ? 'EMPTY FORMAT' : 'format "' . $param_hash->{'format'} . '"';
                    } else {
                        print 'MISSING format';
                    }
                } elsif ($param_hash->{'type'} eq 'text') {
                    if (exists($param_hash->{'value'}) and looks_like_number($param_hash->{'value'})) {
                        print ', value "', $param_hash->{'value'}, '" SHOULD THIS BE ';
                        print int($param_hash->{'value'}) == $param_hash->{'value'} ? "integer OR float?" : "float?";
                    }
                    if (exists($param_hash->{'optional'}) and lc($param_hash->{'optional'}) eq 'true' and !exists($param_hash->{'validator'})) {
                        print ', REMOVE optional="true", it is redundant for a text param without a <validator>';
                    }
                } elsif ($param_hash->{'type'} eq 'boolean' and exists($param_hash->{'optional'}) and lc($param_hash->{'optional'}) eq 'true') {
                    print ', REMOVE optional="true"';
                } elsif ($param_hash->{'type'} eq 'integer') {
                    if (! exists($param_hash->{'value'})) {
                        if (! exists($param_hash->{'optional'}) or lc($param_hash->{'optional'}) eq 'false') {
                            print ', MISSING value';
                        }
                    } elsif ($param_hash->{'value'} ne '' and (! looks_like_number($param_hash->{'value'}) or int($param_hash->{'value'}) != $param_hash->{'value'})) {
                        print ', value "', $param_hash->{'value'}, '" IS NOT AN integer';
                    }
                } elsif ($param_hash->{'type'} eq 'float') {
                    if (! exists($param_hash->{'value'})) {
                        if (! exists($param_hash->{'optional'}) or lc($param_hash->{'optional'}) eq 'false') {
                            print ', MISSING value';
                        }
                    } elsif ($param_hash->{'value'} ne '' and ! looks_like_number($param_hash->{'value'})) {
                        print ', value "', $param_hash->{'value'}, '" IS NOT A float'
                    }
                } elsif ($param_hash->{'type'} eq 'select') {
                    checkSelectParam($param_name, $param_hash);
                }
            }
            if ($param_hash->{'type'} ne 'select' and exists($param_hash->{'optional'}) and lc($param_hash->{'optional'}) eq 'false') {
                print ', REMOVE REDUNDANT optional="false"';
            }
            print "\n";
            if (exists($param_hash->{'validator'})) {
                # $param_hash->{'validator'} is an array reference
                foreach my $validator_hash (@{ $param_hash->{'validator'} }) {
                    if (! exists($validator_hash->{'type'})) {
                        print "-- MISSING VALIDATOR type\n"
                    } else {
                        if ($validator_hash->{'type'} eq 'regex' and ! exists($validator_hash->{'message'})) {
                            print "-- MISSING REGEX VALIDATOR message\n"
                        }
                    }
                }
            }
        }
    }

    # Check <repeat>s and recurse
    if (exists($param_container->{'repeat'})) {
        # $param_container->{'repeat'} is a hash reference
        foreach my $repeat_hash (values %{ $param_container->{'repeat'} }){
            checkParamContainer($repeat_hash);
        }
    }

    # Check <when>s in <conditional>s and recurse
    if (exists($param_container->{'conditional'})) {
        # $param_container->{'conditional'} is a hash reference
        while (my ($conditional_name, $conditional_hash) = each(%{ $param_container->{'conditional'} })) {
            if (! exists($conditional_hash->{'param'}) or scalar keys %{ $conditional_hash->{'param'} } != 1) {
                print "CONDITIONAL $conditional_name MUST HAVE EXACTLY 1 <param> CHILD\n";
            } else {
                my ($conditional_param_name, $conditional_param_hash) = each(%{ $conditional_hash->{'param'} });
                print '- ', $conditional_param_name, ': ';
                if ($conditional_param_hash->{'type'} ne 'select') {
                    print "MUST BE OF TYPE \"select\"\n";
                } else {
                    print 'type "', $conditional_param_hash->{'type'}, '"';
                    checkSelectParam($conditional_param_name, $conditional_param_hash);
                    print "\n";
                    my @select_options = keys %{ $conditional_param_hash->{'option'} };
                    foreach my $select_option (@select_options) {
                        if (! exists($conditional_hash->{'when'}->{$select_option})) {
                            print "CONDITIONAL $conditional_name: MISSING <when> FOR <option value=\"$select_option\">\n";
                            next;
                        }
                        checkParamContainer($conditional_hash->{'when'}->{$select_option});
                    }
                }
            }
        }
    }
}


sub checkXML {
    # Create object
    my $xml = new XML::Simple;
    
    my $infile = $_[0];
    print "Checking file: $infile\n";
    my $data = $xml->XMLin($infile, KeyAttr => {conditional => 'name', data => 'name', option => 'value', param => 'name', repeat => 'name', when => 'value'}, ForceArray => ['conditional', 'data', 'param', 'repeat', 'requirement', 'validator', 'when']);
    
    # Print complete XML structure
    #print Dumper($data);
    
    # Check ID, name and version
    print 'Tool ID: ';
    if (exists($data->{id})) {
        print $data->{id};
        print ' NOT COMPLIANT' if ($data->{id} !~ m/^[a-z0-9_]+$/); # see http://wiki.galaxyproject.org/Admin/Tools/ToolConfigSyntax#A.3Ctool.3E_tag_set
    } else {
        print 'MISSING';
    }
    print "\n";
    print 'Tool name: ', exists($data->{name}) ? $data->{name} : 'MISSING', "\n";
    print 'Tool version: ', exists($data->{version}) ? $data->{version} : 'MISSING', "\n";
    
    # Check requirements
    print 'Requirements:';
    if (exists($data->{'requirements'}) and exists($data->{'requirements'}->{'requirement'})) {
        print "\n";
        # $data->{'requirements'}->{'requirement'} is an array reference
        foreach my $hash_ref (@{ $data->{'requirements'}->{'requirement'} }) {
            print '- ';
            print exists(${$hash_ref}{'content'}) ? ${$hash_ref}{'content'} : 'MISSING content', ': ';
            if (exists(${$hash_ref}{'type'})) {
                print 'type "', ${$hash_ref}{'type'}, '"';
                if (${$hash_ref}{'type'} eq 'package') {
                    print ', ', exists(${$hash_ref}{'version'}) ? 'version ' . ${$hash_ref}{'version'} : 'MISSING version';
                }
            } else {
                print 'MISSING type';
            }
            print "\n";
        }
    } else {
        print " NONE\n";
    }
    
    # Check version_command
    print 'Version command: ', exists($data->{'version_command'}) ? $data->{'version_command'} : 'NONE', "\n";
    
    # Check inputs
    print 'Inputs:';
    if (exists($data->{'inputs'})) {
        print "\n";
        checkParamContainer($data->{'inputs'});
    } else {
        print " MISSING\n";
    }
    
    # Check outputs
    print 'Outputs:';
    if (exists($data->{'outputs'})) {
        if (exists($data->{'outputs'}->{'data'})) {
            # $data->{'outputs'}->{'data'} is a hash reference
            print "\n";
            my $missing_label_message = scalar keys %{ $data->{'outputs'}->{'data'} } > 1 ? 'MISSING label' : 'default label';
            while (my ($data_name, $data_hash) = each(%{ $data->{'outputs'}->{'data'} })) {
                print '- ', $data_name, ': ';
                print exists($data_hash->{'format'}) ? 'format "' . $data_hash->{'format'} . '"' : 'MISSING format', ', ';
                print exists($data_hash->{'label'}) ? 'label "' . $data_hash->{'label'} . '"' : $missing_label_message, "\n";
            }
        } else {
            print " NONE\n";
        }
    } else {
        print " MISSING\n";
    }
    
    # Check help
    if (exists $data->{'help'}) {
        my $help = $data->{'help'};
        if ($help =~ m/License and citation\*\*(.*)/isg) {
        } else {
            print "Help: MISSING License and citation SECTION\n";
        }
    } else {
        print "Help: MISSING\n";
    }
    
    print "\n";
}


sub recurse {
    my($path) = @_; 
    
    #print( "working in: $path\n" );
    
    # append a trailing / if it's not there 
    $path .= '/' if ($path !~ /\/$/); 
    
    # loop through the files contained in the directory 
    for my $eachFile (glob($path . '*')) {
        # if the file is a directory 
        if (-d $eachFile) {
            # pass the directory to the routine ( recursion )
            recurse($eachFile);
        } else {
            # Keep only files ending in .xml
            next unless ($eachFile =~ m/\.xml$/);
            next if ($eachFile =~ m/\/datatypes_conf.xml/ or $eachFile =~ m/\/tool_dependencies.xml/ or $eachFile =~ m/\/repository_dependencies.xml/);
            print STDERR "Processing $eachFile\n";
            checkXML($eachFile);
        } 
    } 
}
