"""



"""



import os
import sys
import string
import re
import argparse
import time

class FastaStream:

	def __init__(self, multifastafile):
		"""
		Generate a stream of 'FASTA strings' from an io stream.
		"""
		self.infile = open(multifastafile)
		self.buffer = []

	def __del__(self) :
		self.infile.close()

	def __iter__(self):
		return self

	def next(self):
		while 1:
			try:
				line = self.infile.next()
				if line.startswith('>') and self.buffer:
					fasta = "".join(self.buffer)
					self.buffer = [line]
					return fasta
				else:
					self.buffer.append(line)
			except StopIteration:
				if self.buffer:
					fasta = "".join(self.buffer)
					self.buffer = []
					return fasta
				else:
					raise StopIteration

		
# END OF CLASS










class DiagnosticDraft:
	
	def __init__(self, infilename):
		self.infilename = infilename.strip()
		self.infile = open(infilename.strip(), 'r')
		self.outfilename = infilename + ".summary"
		self.gaps = re.compile('[Nn]+')
		self.lowqual = re.compile('[acgt]+')
		self.contig = re.compile('[ACGT]+')
		#self.contig = re.compile('[ACGTacgt]+')
		
	def analyze(self):
		""" """
		self._acquire()
		self._cleanEnvironment()
		for chromosome in self.genome:
			self.header = chromosome
			self.fasta = self.genome[chromosome]
			self._setStatistics()
			self._setIterators()
			self._setOutGeneralStatistics()
		return
			
	def _cleanEnvironment(self):
		""" """
		os.popen("rm -f %s.*.interval" % self.infilename)
		os.popen("rm -f %s" % self.outfilename)
		return
		
	def _acquire(self):
		"""	 """
		self.genome = {}
		fastastream = FastaStream(self.infilename)
		for chromosome in fastastream:
			header = chromosome.split("\n")[0].replace(">", "")
			fastaseq = "".join(chromosome.split("\n")[1:])
			self.genome[header] = fastaseq
		return
		
	def _setStatistics(self):
		""" """
		self.contigsstatistics = self.contig.findall(self.fasta)
		self.contigslength = []
		for contig in self.contigsstatistics:
			self.contigslength.append(len(contig))
		
		self.gapsstatistics = self.gaps.findall(self.fasta)
		self.gapslength = []
		for gap in self.gapsstatistics:
			self.gapslength.append(len(gap))
		
		self.lowqualstatistics = self.lowqual.findall(self.fasta)
		self.lowquallength = []
		for lowqual in self.lowqualstatistics:
			self.lowquallength.append(len(lowqual))
		return		

	def _setIterators(self):
		""" """
		self.contigiterator = self.contig.finditer(self.fasta)
		self.gapsiterator	= self.gaps.finditer(self.fasta)
		self.lowqualiterator = self.lowqual.finditer(self.fasta)
		return
		
	def _setOutGeneralStatistics(self):
		""" """
		ns = self.fasta.count("n")
		nsperc =  float(ns)/ float(len(self.fasta)) * 100
		if self.lowquallength == []: self.lowquallength = [0]
		if self.gapslength == []: self.gapslength = [0]
		 
		outline = "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s"	 % (
			"# SAMPLE: " + self.infilename+ " chromosome: "+ self.header, \
			"# Assembled:\t" + str(self._basesAssembled(self.contigslength)), \
			"# Contigs:\t" + str(len(self.contigsstatistics)), \
			"# Contigs>2k:\t" + str(self._calcOver2000(self.contigslength)), \
			"# ContigsAverage:\t" + str(self._calcAverage(self.contigslength)), \
			"# ContigMedian:\t" + str(self._calcMedian(self.contigslength)), \
			"# LowQuality:\t" + str(len(self.lowqualstatistics)), \
			"# Gaps:\t" + str(len(self.gapsstatistics)), \
			"# Gaps<5:\t" + str(self._calcGaps5(self.gapslength)), \
			"# GapsAverage:\t" + str(self._calcAverage(self.gapslength)), \
			"# GapsMedian:\t" + str(self._calcMedian(self.gapslength)), \
			"# GapsMax:\t" + str(max(self.gapslength)), \
			"# N50:\t" + str(self._calcN50(self.contigslength)))

		print outline
		return

	def _setOutIntervals(self):
		""" """ 
		out = open(self.infilename+ ".gap.interval", "a")
		for element in self.gapsiterator:
			gapseq = element.group()
			gapstart = str(element.span()[0] +1)
			gapstop = str(element.span()[1] +1)
			try: gappre = self.fasta[element.span()[0]-50: element.span()[0]] # gestire le eccezioni
			except: gappre = self.fasta[: element.span()[0]]
			try: gappost = self.fasta[element.span()[1]: element.span()[1]+ 50]
			except: gappost = self.fasta[element.span()[1]:]
			out.write(self.header +"\t"+ gapstart +"\t"+ gapstop +"\t+\t"+ gappre+ "\t"+ gappost +"\n")
		out.close()
		
		out = open(self.infilename+ ".lowqual.interval", "a")
		for element in self.lowqualiterator:
			lqseq = element.group()
			lqstart = str(element.span()[0] +1)
			lqstop = str(element.span()[1] +1)
			try: lqpre = self.fasta[element.span()[0]-50: element.span()[0]] # gestire le eccezioni
			except: lqpre = self.fasta[: element.span()[0]]
			try: lqpost = self.fasta[element.span()[1]: element.span()[1]+ 50]
			except: lqpost = self.fasta[element.span()[1]:]
			out.write(self.header +"\t"+ lqstart +"\t"+ lqstop +"\t+\t"+ lqpre+ "\t"+ lqpost +"\n")
		out.close()		
		
		out = open(self.infilename+ ".contig.interval", "a")
		for element in self.contigiterator:
			contigseq = element.group()
			contigstart = str(element.span()[0] +1)
			contigstop = str(element.span()[1] +1)
			try: contigpre = self.fasta[element.span()[0]-50: element.span()[0]] # gestire le eccezioni
			except: contigpre = self.fasta[: element.span()[0]]
			try: contigpost = self.fasta[element.span()[1]: element.span()[1]+ 50]
			except: contigpost = self.fasta[element.span()[1]:]
			out.write(self.header +"\t"+ contigstart +"\t"+ contigstop +"\t+\t"+ contigpre+ "\t"+ contigpost +"\n")
		out.close()		
		return

	def _calcAverage(self, values):
		""" """
		total = 0
		for element in values:
			total += element
		try: average = total/float(len(values))
		except: average = 0
		return int(round((average), 1))
		
	def _calcMedian(self, values):
		""" """
		theValues = sorted(values)
		if len(theValues) % 2 == 1:
			return int(theValues[(len(theValues)+1)/2-1])
		else:
			try:
				lower = theValues[len(theValues)/2-1]
				upper = theValues[len(theValues)/2]
				return int((float(lower + upper)) / 2)	
			except:
				lower = 0
				upper = 0
				return int((float(lower + upper)) / 2)	

	def _calcN50(self, values):
		""" """
		theValues = sorted(values)
		theValues.reverse()
		assembled = 0
		for value in values: assembled += value

		index = 0
		tmp50 = 0
		while tmp50 < float(assembled)/2:
			tmp50 += theValues[index]
			index += 1
		try: N50 =  theValues[index]
		except: N50 = 0
		return N50
		
	def _basesAssembled(self, values):
		""" """
		total = 0
		for element in values:
			total += element
		return total

	def _calcOver2000(self, values):
		""" """
		total = 0
		for element in values:
			if element >= 2000: total +=1
		return total

	def _calcGaps5(self, values):
		""" """
		total = 0
		for element in values:
			if element <= 5: total +=1
		return total

### END OF CLASS	
		
class ContigsMerger:
	
	def __init__(self, contigsfilename):
		self.contigsfilename = contigsfilename
		#self.outfilename = contigsfilename.rsplit('.', 1)[0] + '.pseudomol.fasta'
		self.outfilename = contigsfilename + '.pseudomol'
		self.toMap = {}
		
	def merge(self):
		""" """
		self._acquire()
		self._collapseToPseudoMolecule()
		#self._checkNewDraft()
		print "Merged Contigs (%s) into pseudomol : %s" % (self.contigsfilename, self.outfilename)
		return
	
	def _acquire(self):
		""" """
		for fasta in FastaStream(self.contigsfilename):
			title = int(fasta.split("\n")[0].split("_")[0].replace(">", ""))
			sequence = "".join(fasta.split("\n")[1:])
			self.toMap[title] = self._chopTermNs(sequence)	
		return

	def _checkNewDraft(self):
		""" """
		A = DiagnosticDraft(self.outfilename)
		A.analyze()
		return

	def _collapseToPseudoMolecule(self):
		""" """
		out = open(self.outfilename, "w")
		out.write(">pseudomol_%s\n" % self.contigsfilename.rsplit('.', 1)[0])
		tmpline = []
		for title in sorted(self.toMap):
			sequence = self.toMap[title]+ 'N' * 120
			for element in sequence:
				tmpline.append(element)
				if len(tmpline) >= 100:
					out.write("".join(tmpline) + "\n")
					tmpline = []
		out.write("".join(tmpline) + "\n")
		out.close()
		return
	
	def _chopTermNs(self, sequence):
		""" """
		while sequence.endswith('N'):
			sequence = sequence[:-1]
		while sequence.startswith('N'):
			sequence = sequence[1:]
		return sequence

# END OF CLASS

class ContigsMapper:
	
	def __init__(self, targetfile, readsfile, mapper, vcfconversion):
		self.targetfile = targetfile
		self.readsfile = readsfile
		self.mapper = mapper
		self.vcfconversion = vcfconversion
		#self.samfile = targetfile.rsplit(".", 1)[0] + ".mapped.sam"
		#self.bamfile = targetfile.rsplit(".", 1)[0] + ".mapped.bam"
		#self.sortbam = targetfile.rsplit(".", 1)[0] + ".sorted"
		#self.vcffile = targetfile.rsplit(".", 1)[0] + ".sorted.vcf"
		#self.bcffile = targetfile.rsplit(".", 1)[0] + ".sorted.bcf"
		self.samfile = targetfile + ".mapped.sam"
		self.bamfile = targetfile + ".mapped.bam"
		self.sortbam = targetfile + ".sorted"
		self.vcffile = targetfile + ".sorted.vcf"
		self.bcffile = targetfile + ".sorted.bcf"
		self.logfile = targetfile + ".mappin.log"
		
	def run(self):
		""" """
		#os.system('clear')
		print "# Re-mapping..."
		if self.mapper == "BWA":
			self._runBWA()
		elif self.mapper == "BWAMEM":
			self._runBWAMEM()
		elif self.mapper == "BWT2":
			self._runBWT2()
		elif self.mapper == "LASTZ":
			self._runLastZ()
		else:
			print "unrecognized param: -m"
			sys.exit()
		self._runSamTools()
		if self.vcfconversion == "Y":
			self._runVcfTools()
		os.popen("rm *.mapped.sam *.mapped.bam")
		return

	def _runBWA(self):
		""" """
		command = "bwa index %s" % self.targetfile
		os.popen(command)
		command = "bwa aln %s %s > lothar_aln.sai" % (self.targetfile, self.readsfile)
		os.popen(command)
		command = "bwa samse %s lothar_aln.sai %s > %s" % (self.targetfile, self.readsfile, self.samfile)
		os.popen(command)
		return

	def _runBWT2(self):
		""" """
		command = "bowtie2-build -f %s target" % self.targetfile
		os.popen(command)
		command = "bowtie2 --fast -q -x target -U %s -S %s 2>>%s" % (self.readsfile, self.samfile, self.logfile)
		os.popen(command)
		command = "rm *.bt2"
		os.popen(command)
		return

	def _runBWAMEM(self):
		""" """
		command = "bwa index %s "  % self.targetfile
		os.popen(command)
		command = "bwa mem %s %s > %s" % (self.targetfile, self.readsfile, self.samfile)
		os.popen(command)
		return

	def _runLastZ(self):
		""" """
		command = "lastz %s %s --strand=both --ambiguous=iupac --identity=80 --coverage=80 --output=%s --format=sam"  % (self.targetfile, self.readsfile, self.samfile)
		os.popen(command)
		return

	def _runSamTools(self):
		""" """
		command = "samtools faidx %s 2>>%s" % (self.targetfile, self.logfile)
		os.popen(command)
		command = "samtools view -bt %s.fai %s -o %s 2>>%s" % (self.targetfile, self.samfile, self.bamfile, self.logfile)
		os.popen(command)
		command = "samtools sort %s %s 2>>%s" % (self.bamfile, self.sortbam, self.logfile)
		os.popen(command)
		command = "samtools index %s.bam 2>>%s" % (self.sortbam, self.logfile)
		os.popen(command)
		return

	def _runVcfTools(self):
		""" """
		command = "samtools mpileup -uf %s %s.bam > %s 2>>%s" % (self.targetfile, self.sortbam, self.bcffile, self.logfile)  # -Q 0
		os.popen(command)
		command = "bcftools view -cg %s > %s 2>>%s" % (self.bcffile, self.vcffile, self.logfile) # -p 0.9
		os.popen(command)
		#command = "rm $1.mapped.sorted.bcf"
		#os.popen(command)
		return

### END OF CLASS
# END OF CLASS

class IupacToGaps:
	
	def __init__(self, draftfile):
		self.draftfile = draftfile
		#self.outname = draftfile.rsplit('.', 1)[0] + '.noiupac'
		self.outname = draftfile + '.noiupac'
		self.iupac = re.compile('[rymksbdhvw]+')
		
	def convert(self):
		""" """
		os.popen("rm -f %s" % self.outname)
		F = FastaStream(self.draftfile)
		for chromosome in F:
			title = chromosome.split('\n')[0]
			sequence = "".join(chromosome.split('\n')[1:])
			noiupacseq = self._removeIupac(sequence)
			self._setOut(title, noiupacseq)		   
		return
		
	def _removeIupac(self, sequence):
		""" """
		iupac = ['r','y', 'm', 'k', 's', 'b', 'd', 'h', 'v', 'w', \
				'R','Y', 'M', 'K', 'S', 'B', 'D', 'H', 'V', 'W']
		for element in iupac:
			sequence = sequence.replace(element, 'n')
		return sequence
		
	def _setOut(self, title, sequence):
		""" """
		out = open(self.outname, 'a')
		out.write(title +'\n')
		tmpline = ""
		for nucleotide in sequence:
			tmpline += nucleotide
			if len(tmpline) == 100:
				out.write(tmpline + '\n')
				tmpline = ""
		out.write(tmpline + '\n')
		out.close()
		return
		
# END OF CLASS

class RegionExtractor:

	def __init__(self, infilename, minlength, flanking):
		self.infilename = infilename
		#self.outname = infilename.rsplit(".", 2)[0] + ".contigs"
		self.outname = infilename.rsplit(".", 1)[0] + ".contigs"
		self.infile = open(infilename.strip(), 'r')
		self.minlength = int(minlength)
		self.flanking = int(flanking)
		self.pattern = re.compile('[acgtACGT]+')
	
	def extract(self):
		""" """
		self._acquire()
		self._extract()
		return

	def _acquire(self):
		""" """
		# attenzione funziona per un solo cromosoma
		header = self.infile.next().strip()
		self.fasta = ""
		try:
			while True:
				row = self.infile.next().strip()
				self.fasta += row.strip()
		except StopIteration:
			self.fasta.replace("\n", "") # eccessivo
			return

	def _extract(self):
		""" """
		out = open(self.outname, "w")
		contigiterator = self.pattern.finditer(self.fasta)
		counter = 1
		admitted = 0
		lengths = []
		for gap in contigiterator:
			length = len(gap.group())
			sequence = gap.group()
			contigStart = gap.span()[0]
			contigStop = gap.span()[1]
			if length >= self.minlength:
				lengths.append(length)
				head = ">"+ str(counter) + "_"+ str(contigStart) + "_"+str(contigStop)
				out.write(head + "\n"+ sequence.upper() + "\n")
				admitted += 1
			counter += 1
		out.close()
		average = int(float(min(lengths) + max(lengths)) / 2)
		print "# Input File: %s " % self.infilename
		print "# Extracted %i Regions in file : %s" % (admitted, self.outname)
		print "# Average Len: %i " % average
		return


### END OF CLASS

class ContigExtractor:
	
	def __init__(self, draftfile, minLenCont):
		self.draftfile = draftfile
		self.minLenCont = minLenCont
		
	def extract(self):
		""" """
		I = IupacToGaps(self.draftfile)
		I.convert()
		self.draftfile = I.outname
		
		S = RegionExtractor(self.draftfile, self.minLenCont, "0")
		S.extract()
		self.outname = S.outname
		os.popen("rm -f *.noiupac")
		return	


# END OF CLASS


class ContigOverlapChecker:
	
	def __init__(self, contigsfile):#, minLenCont):
		self.contigsfile = contigsfile
		self.contigs = {}
		self.queryfile = contigsfile + ".renamed"
		self.finaloutname = contigsfile + ".renamed.selfblasted"
		
	def check(self):
		""" """
		#S = RegionExtractor(self.draftfile, "0", "0")
		#S.extract()
		self._acquireContigs()
		self._writeContigsOut()
		self._checkNeighbourContigs()
		self._checkAllAgainstAll()
		return	
	
	def _acquireContigs(self):
		""" """
		F = FastaStream(self.contigsfile)
		counter = 1
		for fasta in F:
			sequence = "".join(fasta.split("\n")[1:])
			self.contigs[counter] = sequence
			counter += 1 
		return
	
	def _writeContigsOut(self):
		""" """
		out = open(self.queryfile, 'w')
		for contig in sorted(self.contigs):
			sequence = self.contigs[contig]
			out.write(">"+str(contig)+"\n"+sequence+"\n")
		out.close()
		return

	def _checkNeighbourContigs(self):
		""" """
		print "Contig Overlap Checking"
		for contigIndex in sorted(self.contigs)[:-1]:
			contig = self.contigs[contigIndex]
			follow = self.contigs[contigIndex+ 1]
			tail = contig[-40:-5]
			extended = contig[-70:-5]
			elonged = contig[-70:]
			if tail in follow:
				if extended in follow:
					print "check :", contigIndex, " and ", contigIndex+1, "extended: ", follow.find(extended), extended[:40]
				else:
					print "check :", contigIndex, " and ", contigIndex+1, "tail: ", follow.find(tail), tail[:40]		
		return
	
	def _checkAllAgainstAll(self):
		""" """
		print "Blasting All VS All"
		self._runOut()
		self._parseBlastOut()		
		return
	
	def _runOut(self):
		""" """
		tmpoutname = self.queryfile+".tmp.blast"
		command = 'blastn -query %s -subject %s -task megablast -evalue 0.1 -out %s -outfmt "6 std nident qlen slen" -strand both -max_target_seqs 10 -ungapped'  % (self.queryfile, self.queryfile, tmpoutname)
		os.popen(command)
		return
	
	def _parseBlastOut(self):
		""" """
		out = open(self.finaloutname, 'w')
		infile = open(self.queryfile+".tmp.blast")
		try:
			while True:
				row = infile.next().strip()
				cols = row.split("\t")
				if cols[0].strip() != cols[1].strip() and int(cols[4]) < 7 and int(cols[3]) > 100:
					out.write(row + '\n')
		except StopIteration:
			infile.close()
			out.close()
			os.remove(self.queryfile+".tmp.blast")
			print "Blast Resuslts in %s file" % self.finaloutname
			return
						

#################################


# END OF CLASS
class ContigsFixer:
	
	def __init__(self, contigsfile, readsfile):#, outfilename):
		self.contigsfile = contigsfile
		self.readsfile = readsfile
		#self.vcffile = vcffile
		#self.outfilename = draftfile.rsplit('.', 1)[0] + ".repaired"
		self.outfilename = contigsfile + ".repaired"
		self.mindp = 20
		self.minmq = 15
		self.minbaf = 0.61 # parametrizzare
		self.indels = {}
		self.polymorph = {}
		self.mapped = []
		
	def repair(self):
		""" """
		start = time.time()
		print "# Draft Doctor. Reparing Contigs from vcf"
		self._convertContigsToPseudomol()
		self._contigsMapper()
		self._parseVcf()
		self._acquireDraft()
		self._repairPolymorphisms()
		#self._repairUnmapped()
		self._repairIndels()
		#self._checkNewDraft()
		self._contigXtractor()
		self._contigsOverlapChecking()
		end = time.time()
		print "total time: ", str(float(end-start)/60), " minutes"
		return

	def _convertContigsToPseudomol(self):
		""" """
		CM = ContigsMerger(self.contigsfile)
		CM.merge()
		self.draftfile = CM.outfilename
		return

	def _contigsMapper(self):
		""" """
		CM = ContigsMapper(self.draftfile, self.readsfile, "BWT2", "Y")
		CM.run()
		self.vcffile = CM.vcffile
		return

	def _contigXtractor(self):
		""" """
		CX = ContigExtractor(self.outfilename, "100")
		CX.extract()
		self.repaired = CX.outname
		return

	def _contigsOverlapChecking(self):
		""" """
		COC = ContigOverlapChecker(self.repaired)
		COC.check()
		return

		
	def _parseVcf(self):
		""" """
		vcffile = open(self.vcffile)
		gtfield = re.compile('[01]/[01]:[\d]+,[\d]+,[\d]+:[\d]+')

		total = 0
		admitted = 0
		snps = 0
		indels = 0
		bafpassing = 0
		dptotal = 0

		try:
			while True:
				row = vcffile.next()
				while row.startswith('#'):
					row = vcffile.next()
				
				pos = row.split('\t')[1]
				ref = row.split('\t')[3]
				alt = row.split('\t')[4]
				index = int(pos) -1
				if index not in self.mapped: self.mapped.append(index)
				
				dp = int(row.split('DP=')[1].split(';')[0])
				if "N" not in ref: dptotal += dp
				try: af = float(row.split('AF1=')[1].split(';')[0])
				except: af = 0
				try: mq = float(row.split('MQ=')[1].split(';')[0])
				except: mq = 0
				try: dp4 = row.split('DP4=')[1].split(';')[0].split(',')
				except: dp4 = ['0', '0', '0', '0']
				# BAF
				try: baf = (float(dp4[2]) + float(dp4[3])) / (float(dp4[0]) + float(dp4[1]) + float(dp4[2]) + float(dp4[3]))
				except: baf = 0
				genotypinginfo = gtfield.search(row)
				if genotypinginfo: genotyping = genotypinginfo.group()
				else: genotyping = ""
				# criteria
				criteria1 = (dp >= self.mindp and mq >= self.minmq) # requisiti minimi di qualita'
				criteria2 = ("." not in alt)                        # base confermata
				criteria3 = ("," not in alt)                        # questo deve essere gestito meglio
				#criteria4 = ("N" not in ref.upper())                # come primo round non deve inserire nuove basi
				if criteria1 and criteria2 and criteria3:
					admitted += 1
					# indel
					if "INDEL" in row:
						indels += 1
						if baf >= self.minbaf:
							#print pos, ref, alt, dp, mq, dp4, baf
							bafpassing += 1
							self.indels[index] = (ref, alt, dp, af)								
								
					# polimorphism
					else:
						snps += 1
						self.polymorph[index] = (ref, alt, dp, af) 
						
				total += 1
					
		except StopIteration:
			print "###################################"
			print "# DraftDoctor Draft Fixing from VCF, file: ", self.vcffile
			print "# Total variants:", total, "of them, satisfying criteria: ", admitted
			print "# SNPs:", snps, "INDELS", indels, "BAF passing", bafpassing
			print "# Average DP: ", str(float(dptotal)/float(total))
			print "###################################"
		
		return
		
	def _acquireDraft(self):
		""" """
		draftfile = open(self.draftfile).read()
		self.drafthead = draftfile.split('\n')[0].strip()
		self.draftsequence = "".join(draftfile.split('\n')[1:])
		iupac = ['R', 'Y', 'M', 'K', 'S', 'B', 'D', 'H', 'V', 'W']
		for element in iupac:
			if element in self.draftsequence:
				self.draftsequence = self.draftsequence.replace(element, 'N')
		return
	
	def _repairPolymorphisms(self):
		""" """
		sequence = list(self.draftsequence)
		for index in self.polymorph:
			ref = self.polymorph[index][0]
			alt = self.polymorph[index][1]
			
			if sequence[index] == ref:
				sequence[index] = alt
			elif sequence[index] == "N":
				sequence[index] = alt
			else:
				print "error in position ", index, index +1, sequence[index], ref, alt
				
		self.draftsequence = "".join(sequence)
		return

	def _repairIndels(self):
		""" """
		sequence = list(self.draftsequence)
		positions = sorted(self.indels.keys())
		for index in reversed(positions):
			ref = self.indels[index][0]
			alt = self.indels[index][1]
			sequence[index: index+len(ref)] = list(alt)
		self.draftsequence = "".join(sequence)
		self._setOutNewDraft()
		return
	
	def _repairUnmapped(self):
		""" """
		chopped = 0
		sequence = list(self.draftsequence)
		for index in range(len(sequence)):
			if index not in self.mapped:
				ref = sequence[index]
				if ref != "N":
					sequence[index] = "N"
					chopped += 1
		self.draftsequence = "".join(sequence)		
		print "total chopped ", chopped 
		
		return
		
	def _setOutNewDraft(self):
		""" """
		out = open(self.outfilename, 'w')
		out.write(self.drafthead + '\n')
		tmpline = []
		for nucleotide in self.draftsequence:
			tmpline.append(nucleotide.upper())
			if len(tmpline) >= 100:
				out.write("".join(tmpline) + '\n')
				tmpline = []
				
		out.write("".join(tmpline) + '\n') 
		out.close()
		return

	def _checkNewDraft(self):
		""" """
		A = DiagnosticDraft(self.outfilename)
		A.analyze()
		
		
		return


#################################


# END OF CLASS
		

	
def main(argv):
	""" main function """
	parser = argparse.ArgumentParser()

	parser.add_argument('-c', dest='contigsfile', action='store', required=True, help='Draft File Mandatory ')
	parser.add_argument('-r', dest='readsfile', action='store', required=True, help='Contigs File Mandatory ')
	#parser.add_argument('-o', dest='outfilename', action='store', required=False, help='Vcf File Mandatory ')
	
	args = parser.parse_args()

	DR = ContigsFixer(args.contigsfile, args.readsfile)#, args.outfilename)
	DR.repair()
	return
	
################
if __name__ == "__main__":
	sys.exit(main(sys.argv))	
		

	
