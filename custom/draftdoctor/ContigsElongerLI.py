

import os
import sys
import string
import re
import argparse
import time

class FastaWriter:
	
	def __init__(self, fastadict, outname, linewidth=100):
		self.fastadict = fastadict
		self.outname = outname
		self.linewidth = linewidth
		
	def writeDown(self):
		""" """
		
		return
	

class FastaStream:

	def __init__(self, multifastafile):
		"""
		Generate a stream of 'FASTA strings' from an io stream.
		"""
		self.infile = open(multifastafile)
		self.buffer = []

	def __del__(self) :
		self.infile.close()

	def __iter__(self):
		return self

	def next(self):
		while 1:
			try:
				line = self.infile.next()
				if line.startswith('>') and self.buffer:
					fasta = "".join(self.buffer)
					self.buffer = [line]
					return fasta
				else:
					self.buffer.append(line)
			except StopIteration:
				if self.buffer:
					fasta = "".join(self.buffer)
					self.buffer = []
					return fasta
				else:
					raise StopIteration

		
# END OF CLASS



class ContigExtensor:
	
	def __init__(self, contigsfile, readsfile, sensitive, rounds):
		self.contigsfile = contigsfile
		self.readsfile = readsfile
		self.sensitive = sensitive
		self.rounds = int(rounds)
		self.finalcontigsname = self.contigsfile +".elonged"
		# files or dictionaries
		self.vcffile = "Contigs.sorted.vcf"
		self.mappingfile = "ContigsToMap.fasta"
		self.mappingContigs = {}
		self.toMap = {}
		# parameters
		self.mindpv = 15  # variant
		self.mindpn = 8   # Ns
		self.minmq = 15
		self.minbaf = 0.7#	0.61 # parametrizzare
		self.forcinggaps = 100
		self.standby = {}
		#
		self.unmapped = []
		return
		
	def extend(self):
		""" """
		start = time.time()
		#os.popen("clear")
		print "# Diagnostic Draft: Contigs Extension"
		self._cleanEnvironment()
		self._acquireRegions()
		for iteration in range(self.rounds):
			self._setRoundDictionaries()
			self._writeContigsToMap(self.mappingfile)
			# mapping
			if self.sensitive == "Y": self._mappingReadsSensitive()
			else: self._mappingReads()
			# scrolling
			self._scrollVcf()
			for contigName in self.vcfData:   self._parseVcfData(contigName)
			# repairing
			for contigName in self.polymorph: self._repairPolymorphisms(contigName)
			for contigName in self.indels:    self._repairIndels(contigName)
			# statistics
			self._checkStatistics(iteration)
			
		self._writeFinalContigs()
		self._cleanEnvironment2()
		end = time.time()
		print "# Draft Doctor. Total time: ", str(float(end-start)/60), " minutes"
		return
	
	def _acquireRegions(self):
		""" """
		contigsStream = FastaStream(self.contigsfile)
		counter = 1
		for contig in contigsStream:
			contigTitle = counter
			contigsSequence = self._iupacToGapconverter("".join(contig.split("\n")[1:]).upper())
			self.toMap[contigTitle] = contigsSequence
			counter += 1
		return			

	def _iupacToGapconverter(self, sequence):
		""" """
		iupac = ['R','Y', 'M', 'K', 'S', 'B', 'D', 'H', 'V', 'W']
		for element in iupac:
			sequence = sequence.replace(element, 'n')
		return sequence		

	
	def _setRoundDictionaries(self):
		""" """
		self.originalcontigs = {}
		self.vcfData = {}
		self.polymorph = {}
		self.indels = {}
		self.repairedContigs = {}
		return
	
	def _writeContigsToMap(self, mappingfile):
		""" """
		tail = "N" * 60
		out = open(mappingfile, "w")
		for contigName in self.toMap:
			if contigName not in self.standby:
				sequence = tail + self.toMap[contigName].strip() + tail
				out.write(">"+str(contigName) + '\n'+ sequence + '\n')
				self.originalcontigs[contigName] = sequence
		out.close()
		return
	
	def _mappingReads(self):
		""" """
		#print "# Mapping Reads"
		bwt2dir = ""# "/usr/local/SFTW/bowtie2/"
		command = bwt2dir +"bowtie2-build -f %s fragmentDB" % (self.mappingfile)
		os.popen(command)
		command = bwt2dir +"bowtie2 --fast -q -a -x fragmentDB -U %s -S %s 2>%s" % (self.readsfile, "Contigs.sam", "Contigs.log")
		os.popen(command)
		command = "samtools faidx %s 2>%s" % (self.mappingfile, "Contigs.log")
		os.popen(command)
		# filt sam
		self._filterSamMap()
		command = "samtools view -bt %s.fai %s -o %s 2>>%s" % (self.mappingfile, "Contigs.sam.filt", "Contigs.bam", "Contigs.log")
		os.popen(command)
		command = "samtools sort %s %s 2>>%s" % ("Contigs.bam", "Contigs.sorted", "Contigs.log")
		os.popen(command)
		command = "samtools index %s.bam 2>>%s" % ("Contigs.sorted", "Contigs.log")
		os.popen(command)
		command = "samtools mpileup -uf %s %s > %s 2>>%s" % (self.mappingfile, "Contigs.sorted.bam", "Contigs.sorted.bcf", "Contigs.log")
		os.popen(command)
		command = "bcftools view -vcg %s > %s 2>>%s" % ("Contigs.sorted.bcf", "Contigs.sorted.vcf", "Contigs.log")
		os.popen(command)
		#command = "rm -f *.bt2 %s %s %s 2>>%s" % ("Contigs.sam", "Contigs.bam", "Contigs.sorted.bcf", "Contigs.log")
		#os.popen(command)
		return

	def _mappingReadsSensitive(self):
		""" """
		#print "# Mapping Reads"
		bwt2dir = ""# "/usr/local/SFTW/bowtie2/"
		command = bwt2dir +"bowtie2-build -f %s fragmentDB" % (self.mappingfile)
		os.popen(command)
		command = bwt2dir +"bowtie2 --very-sensitive -q -a -x fragmentDB -U %s -S %s 2>>%s" % (self.readsfile, "Contigs.sam", "Contigs.log")
		os.popen(command)
		command = "samtools faidx %s 2>%s" % (self.mappingfile, "Contigs.log")
		os.popen(command)
		# filt sam
		self._filterSamMap()		
		command = "samtools view -bt %s.fai %s -o %s 2>>%s" % (self.mappingfile, "Contigs.sam.filt", "Contigs.bam", "Contigs.log")
		os.popen(command)
		command = "samtools sort %s %s 2>>%s" % ("Contigs.bam", "Contigs.sorted", "Contigs.log")
		os.popen(command)
		command = "samtools index %s.bam 2>>%s" % ("Contigs.sorted", "Contigs.log")
		os.popen(command)
		command = "samtools mpileup -uf %s %s > %s -Q 0 2>>%s" % (self.mappingfile, "Contigs.sorted.bam", "Contigs.sorted.bcf", "Contigs.log") # 
		os.popen(command)
		command = "bcftools view -cg %s > %s -p 0.9 2>>%s" % ("Contigs.sorted.bcf", "Contigs.sorted.vcf", "Contigs.log") # 
		os.popen(command)
		#command = "rm -f *.bt2 %s %s %s 2>>%s" % ("Contigs.sam", "Contigs.bam", "Contigs.sorted.bcf", "Contigs.log")
		#os.popen(command)
		return
	
	def _scrollVcf(self):
		""" """
		vcffile = open(self.vcffile)
		try:
			while True:
				row = vcffile.next()
				while row.startswith('#'):
					row = vcffile.next()
				contigName = int(row.split('\t')[0])
				self.vcfData.setdefault(contigName, []).append(row.strip())
		except StopIteration:
			for contigName in self.toMap:
				if contigName not in self.vcfData:
					#print "contig", contigName, "unmapped, not in vcffile"
					self.unmapped.append(contigName)
					self.standby[contigName] = self.toMap[contigName]
			return		
		
	def _parseVcfData(self, contigName):
		""" """
		gtfield = re.compile('[01]/[01]:[\d]+,[\d]+,[\d]+:[\d]+')
		self.indels[contigName] = {}
		self.polymorph[contigName] = {}
		
		for vcfLine in self.vcfData[contigName]:
			pos = vcfLine.split('\t')[1]
			ref = vcfLine.split('\t')[3]
			alt = vcfLine.split('\t')[4]
			index = int(pos) -1
			dp = int(vcfLine.split('DP=')[1].split(';')[0])
			try:
				af = float(vcfLine.split('AF1=')[1].split(';')[0])
				mq = float(vcfLine.split('MQ=')[1].split(';')[0])
				dp4 = vcfLine.split('DP4=')[1].split(';')[0].split(',')
				# BAF
				baf = (float(dp4[2]) + float(dp4[3])) / (float(dp4[0]) + float(dp4[1]) + float(dp4[2]) + float(dp4[3]))
			except:
				af = 0; mq = 0; dp = 0; baf = 0
				#if alt != ".": print ref, alt, dp, dp4, af, mq, baf
				
			# mandrake inserire un controllo sul PL
			if "," in alt:
				alt = alt.split(",")[0].strip()
			# end of mandrake
			
			genotypinginfo = gtfield.search(vcfLine)
			if genotypinginfo: gt = genotypinginfo.group()
			else: gt = ""
			# criteria
			criteria1 = (dp >= self.mindpn and mq >= self.minmq) # requisiti minimi di qualita'
			criteria2 = ("." not in alt)                        # base confermata
			criteria3 = ("," not in alt)                        # questo deve essere gestito meglio

			
			if criteria1 and criteria2:# and criteria3 :
				if "INDEL" in vcfLine:
					if baf >= self.minbaf and dp >= self.mindpv:
						self.indels[contigName][index] = (ref, alt, dp, af, gt)								
				else:
					self.polymorph[contigName][index] = (ref, alt, dp, af, gt) 
		return		

	def _repairPolymorphisms(self, contigName):
		""" """
		# aggiungere un check sul genotyping
		sequence = list(self.originalcontigs[contigName])
		for index in self.polymorph[contigName]:
			ref = self.polymorph[contigName][index][0]
			alt = self.polymorph[contigName][index][1]
			dp = self.polymorph[contigName][index][2]
				
			cond1 = (ref != "N" and dp >= self.mindpv and index >= 20)
			cond2 = (ref == "N" and dp >= self.mindpn)
			if cond1 or cond2:
				try:
					sequence[index] = alt
				except:
					print index, len(sequence), contigName, "error"
					print self.polymorph[contigName].keys()
					sys.exit()
					#pass
		self.repairedContigs[contigName] = "".join(sequence)
		return
	
	def _repairIndels(self, contigName):
		""" """
		sequence = list(self.repairedContigs[contigName])
		positions = sorted(self.indels[contigName].keys())
		for index in reversed(positions):
			if index >= 15: # and index <= (len(sequence) - 100):
				# inserire qui un check sugli indels baf e mindp sono gia' passati
				ref = self.indels[contigName][index][0]
				alt = self.indels[contigName][index][1]
				sequence[index: index+len(ref)] = list(alt)
		self.repairedContigs[contigName] = "".join(sequence)
		return
	
	def _checkStatistics(self, iteration):
		""" """
		extended = 0
		unextended = 0
		fixed = 0
		
		for contigName in self.repairedContigs:
			repairedSeq = self.repairedContigs[contigName]
			mappedSeq = self.originalcontigs[contigName]
			mappedNts = len(mappedSeq) - mappedSeq.count("N")
			repairedNts = len(repairedSeq) - repairedSeq.count("N")
			
			if (repairedNts - mappedNts) > 0:              # estesa
				extended += repairedNts - mappedNts
				self.toMap[contigName] = self._choppingSeq(repairedSeq.upper()) # pronta per il prossimo round
			else:
				if mappedSeq != repairedSeq:
					#print "modificata non estesa"
					self.toMap[contigName] = self._choppingSeq(repairedSeq.upper())
					fixed += 1
				else:
					self.standby[contigName] = self._choppingSeq(repairedSeq.upper())
					del self.toMap[contigName]  # era presente dal round precedente
				unextended += 1
		
		print "# Round: %i. Extended %i, Fixed %i NTs over %i Contigs of %i Mapped. Unextended: %i" % (iteration+1, extended, fixed, len(self.repairedContigs), len(self.originalcontigs), unextended)
		
		if extended == 0 and fixed == 0:
			self._writeFinalContigs()
			print "UnExpected End, No more Extended Nucleotides."
			print "Final Contigs in %s file" % self.finalcontigsname
			sys.exit()
		return	
	
	def _choppingSeq(self, sequence):
		""" """
		while sequence.startswith("N"):
			sequence = sequence[1:]
		while sequence.endswith("N"):
			sequence = sequence[:-1]		
		return sequence

	def _writeFinalContigs(self):
		""" """
		out = open(self.finalcontigsname, "w")
		for title in sorted(self.standby):
			self.toMap[title] = self.standby[title]
		for title in sorted(self.toMap):
			out.write('>'+str(title) + '\n'+ self.toMap[title]+ '\n')
		out.close()
		return

		
	def _revcompl(self, sequence):
		""" """
		dna = {'A':'T', 'C':'G', 'T':'A', 'G':'C', 'N':'N'}
		complement = []
		for nucleotide in sequence:
			complement.append(dna[nucleotide])
		revcompl = reversed(complement)
		return "".join(revcompl)
	

	def _cleanEnvironment(self):
		""" """
		try:
			os.popen("rm -f %s" % "Contigs.sorted.vcf")
			os.popen("rm -f %s" % "ContigsToMap.fasta")
		except:
			pass
		return

	def _cleanEnvironment2(self):
		""" """
		os.popen("rm *.bam *.sam *.bcf *.vcf")
		#try:
		#	os.popen("rm -f Contigs.sorted.*" )
		#	os.popen("rm -f Contigs.log" )
		#	os.popen("rm -f ContigsToMap.*" )
		#except:
		#	pass
		return
		
	def _filterSamMap(self):
		""" """
		maxmismatches = 3
		unmatched = re.compile(r'[ACGT]')
		#print "# Mapping Reads: filtering sam"
		samfile = open("Contigs.sam")
		head = samfile.next()
		row = samfile.next()
		out = open("Contigs.sam.filt", 'w')
		while row.startswith("@"):
			head += row
			row = samfile.next()
		#row = samfile.next() # "@PG"
		head += row
		out.write(head)
		try:
			while True:
				row = samfile.next()
				target = row.split("\t")[2]
				md = row.split('\t')[-2]
				mismatch = len(unmatched.findall(md.split(':')[-1]))
				if target != "*" and mismatch < maxmismatches:
					out.write(row)
		except StopIteration:
			pass
		out.close()
		return
	




#################################


# END OF CLASS
	

	
def main(argv):
	""" main function """
	parser = argparse.ArgumentParser()

	parser.add_argument('-c', dest='contigsfile', action='store', required=True, help='Draft File Mandatory ')
	parser.add_argument('-r', dest='readsfile', action='store', required=True, help='Contigs File Mandatory ')
	parser.add_argument('-s', dest='sensitive', action='store', required=False, default="N", help='Vcf File Mandatory ')
	parser.add_argument('-i', dest='rounds', action='store', required=False, default="2", help='Vcf File Mandatory ')
	
	args = parser.parse_args()

	CE = ContigExtensor(args.contigsfile, args.readsfile, args.sensitive, args.rounds)
	CE.extend()
	return
	
	
################
if __name__ == "__main__":
	sys.exit(main(sys.argv))	
	
