# -*- coding: utf-8 -*-
"""
BEAGLE
version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import os
import shutil
import subprocess
import sys
import time


def __main__():


    # load arguments
    print 'Parsing BEAGLE input options...'
    parser = optparse.OptionParser()

    parser.add_option('--jar',dest='jar')
    parser.add_option('--input_type', dest='input_type')
    parser.add_option('--input_file', dest='input_file')
    parser.add_option('--ref_file', dest='ref_file')
    parser.add_option('--ped_file', dest='ped_file')
    
    parser.add_option('--impute', action="store_true", dest='impute')

    parser.add_option('--exclude_samples_file', dest='exclude_samples_file')
    parser.add_option('--exclude_markers_file', dest='exclude_markers_file')

    parser.add_option('--chrom', dest='chrom')
    parser.add_option('--max_lr', dest='max_lr')
    parser.add_option('--window', dest='window')
    parser.add_option('--overlap', dest='overlap')

    parser.add_option('--gprobs', action="store_true", dest='gprobs')
    parser.add_option('--use_phase', action="store_true", dest='use_phase')

    parser.add_option('--seed', dest='seed')

    parser.add_option('--single_scale', dest='single_scale')
    parser.add_option('--duo_scale', dest='duo_scale')
    parser.add_option('--trio_scale', dest='trio_scale')

    parser.add_option('--burnin_its', dest='burnin_its')
    parser.add_option('--phase_its', dest='phase_its')
    parser.add_option('--impute_its', dest='impute_its')

    parser.add_option('--ibd', action="store_true", dest='ibd')
    parser.add_option('--ibd_lod', dest='ibd_lod')
    parser.add_option('--ibd_scale', dest='ibd_scale')
    parser.add_option('--ibd_trim', dest='ibd_trim')

    parser.add_option('--dump_file', dest='dump_file')
    parser.add_option('--n_samples', dest='n_samples')
    parser.add_option('--build_window', dest='build_window')
    
    parser.add_option('--logfile', dest='logfile')
    parser.add_option('--output_war', dest='output_war')
    parser.add_option('--output_vcf', dest='output_vcf')
    parser.add_option('--output_hbd', dest='output_hbd')
    parser.add_option('--output_ibd', dest='output_ibd')

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build BEAGLE command to be executed

    ex_path = '%s' % options.jar

    if options.input_type == 'gt':
        input_file = 'gt=%s' % (options.input_file)
    elif options.input_type == 'gl':
        input_file = 'gl=%s' % (options.input_file)
    elif options.input_type == 'gtgl':
        input_file = 'gtgl=%s' % (options.input_file)
    else:
        print 'Input file not correctly specified!'

    ref_file = 'ref=%s' % (options.ref_file) if options.ref_file is not None else ''
    ped_file = 'ped=%s' % (options.ped_file) if options.ped_file is not None else ''

    prefix = 'beagle'
    out = 'out=%s' % prefix

    impute = 'impute=true' if options.impute is not None else 'impute=false'

    exclude_samples_file = 'excludesamples=%s' % (options.exclude_samples_file) if options.exclude_samples_file is not None else ''
    exclude_markers_file = 'excludemarkers=%s' % (options.exclude_markers_file) if options.exclude_markers_file is not None else ''

    chrom = 'chrom=%s' % (options.chrom) if options.chrom is not None else ''
    max_lr = 'maxlr=%s' % (options.max_lr) if options.max_lr is not None else ''

    n_threads = '' # = '=%s' % (options.) if options. is not None else ''
    window = 'window=%s' % (options.window) if options.window is not None else ''
    overlap = 'overlap=%s' % (options.overlap) if options.overlap is not None else ''

    gprobs = 'gprobs=true' if options.gprobs is not None else 'gprobs=false'
    use_phase = 'usephase=true' if options.use_phase is not None else 'usephase=false'

    seed = 'seed=%s' % (options.seed) if options.seed is not None else ''

    single_scale = 'singlescale=%s' % (options.single_scale) if options.single_scale is not None else ''
    duo_scale = 'duoscale=%s' % (options.duo_scale) if options.duo_scale is not None else ''
    trio_scale = 'trioscale=%s' % (options.trio_scale) if options.trio_scale is not None else ''

    burnin_its = 'burnin-its=%s' % (options.burnin_its) if options.burnin_its is not None else ''
    phase_its = 'phase-its=%s' % (options.phase_its) if options.phase_its is not None else ''
    impute_its = 'impute-its=%s' % (options.impute_its) if options.impute_its is not None else ''

    ibd = 'ibd=true' if options.ibd is not None else 'ibd=false'
    ibd_lod = 'ibdlod=%s' % (options.ibd_lod) if options.ibd_lod is not None else ''
    ibd_scale = 'ibdscale=%s' % (options.ibd_scale) if options.ibd_scale is not None else ''
    ibd_trim = 'ibdtrim=%s' % (options.ibd_trim) if options.ibd_trim is not None else ''

    dump_file = 'dump=%s' % (options.dump_file) if options.dump_file is not None else ''
    n_samples = 'nsamples=%s' % (options.n_samples) if options.n_samples is not None else ''
    build_window = 'buildwindow=%s' % (options.build_window) if options.build_window is not None else ''


    # output file(s)
    logfile = options.logfile
    output_war = options.output_war
    output_vcf = options.output_vcf

    output_hbd = options.output_hbd if options.output_hbd is not None else ''
    output_ibd = options.output_ibd if options.output_ibd is not None else ''


    # Build BEAGLE command
    cmd = 'java -Xmx1024m -jar %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (ex_path, input_file, ref_file, ped_file, impute, exclude_samples_file, exclude_markers_file, chrom, max_lr, window, overlap, gprobs, use_phase, seed, single_scale, duo_scale, trio_scale, burnin_its, phase_its, impute_its, ibd, ibd_lod, ibd_scale, ibd_trim, dump_file, n_samples, build_window, out)
    print '\nBEAGLE command to be executed:\n %s' % (cmd)

    # Execution of BEAGLE
    print 'Executing BEAGLE...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because BEAGLE writes some logging info there

    finally:

        if log != sys.stdout:
            log.close()

    print 'BEAGLE executed!'

    if os.path.isfile(prefix + ".warnings"):
        shutil.move(prefix + ".warnings", output_war)
    if os.path.isfile(prefix + ".ibd"):
        shutil.move(prefix + ".ibd", output_ibd)
    if os.path.isfile(prefix + ".hbd"):
        shutil.move(prefix + ".hbd", output_hbd)    
    if os.path.isfile(prefix + ".vcf.gz"):
        cmd_gz = 'gunzip %s.vcf.gz' % prefix
        subprocess.check_call(cmd_gz, shell=True)
        shutil.move(prefix + ".vcf", output_vcf)

if __name__ == "__main__":
    __main__()
